package de.fhswf.in.pm;
/**
 * Auflistung der Verschiedenen Pr�fzifferverfahren.
 *
 * @author Tobias Holke {@literal holke.tobias@fh-swf.de} on 28.03.19.
 */
public enum Verfahren
{
   QUERSUMME,
   MULTIPLIKATION
}