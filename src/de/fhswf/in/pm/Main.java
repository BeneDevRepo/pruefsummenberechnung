package de.fhswf.in.pm;

/**
 * Programm zur Berechnung verschiedener Pr�fsummen.
 *
 * @author Tobias Holke
 */
public class Main
{

   public static void main(String[] args)
   {
      Konto meinErstesKonto = new Konto("1234567891", "12345678", "Tobias Holke", Verfahren.MULTIPLIKATION);

      System.out.println("Meine IBAN: "+meinErstesKonto.getIban());
   }
}