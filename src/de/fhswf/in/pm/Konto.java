package de.fhswf.in.pm;

import de.fhswf.in.pm.pruefziffern.MultiplikationPruefziffer;
import de.fhswf.in.pm.pruefziffern.Pruefziffer;
import de.fhswf.in.pm.pruefziffern.QuersummenPruefziffer;

/**
 * Abbildung eines Bankkontos.
 *
 * @author Tobias Holke {@literal holke.tobias@fh-swf.de} on 27.03.19.
 */
public class Konto
{
   private String knummer;
   private String blz;
   private Pruefziffer pruefziffer;
   private String inhaber;

   public Konto(String knummer, String blz, String inhaber, Verfahren verfahren)
   {
      if (knummer == null || knummer.isEmpty())
      {
         throw new IllegalArgumentException("Es muss eine Kontonummer �bergeben werden.");
      }

      if (!knummer.matches("\\d{10}"))
      {
         throw new IllegalArgumentException(knummer + " ist keine g�ltige Kontonummer. Es m�ssen 10 Ziffern sein.");
      }

      if (blz == null || blz.isEmpty())
      {
         throw new IllegalArgumentException("Es muss eine Bankleitzahl angegeben werden.");
      }

      if (!blz.matches("\\d{8}"))
      {
         throw new IllegalArgumentException(blz + " ist keine g�ltige Bankleitzahl. Es m�ssen 8 Ziffern sein.");
      }

      if (inhaber == null || inhaber.isEmpty())
      {
         throw new IllegalArgumentException("Es muss ein Inhaber angegeben werden.");
      }

      this.knummer = knummer;
      this.blz = blz;
      this.inhaber = inhaber;

      if(verfahren.equals(Verfahren.MULTIPLIKATION)) 
      {
    	  pruefziffer = new MultiplikationPruefziffer(knummer, blz);
      }
      else 
      {
    	pruefziffer = new QuersummenPruefziffer(knummer, blz);  
      }
      
      pruefziffer.berechnePruefziffer();
   }

   /**
    * Gibt die Kontonummer und Bankleitzahl des Kontobesitzers im IBAN-Format zur�ck.
    *
    * @return Kontonummer und Bankleitzahl im deutschen IBAN-Format.
    */
   public String getIban()
   {
      return "DE" + pruefziffer.getPruefziffer() + " " + knummer + " " + blz;
   }
}

