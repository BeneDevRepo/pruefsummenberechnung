package de.fhswf.in.pm.pruefziffern;

/**
 * @author Tobias Holke {@literal holke.tobias@fh-swf.de} on 27.03.19.
 */
public class QuersummenPruefziffer implements Pruefziffer
{
   private String knummer;
   private String blz;
   private int pruefziffer;

   public QuersummenPruefziffer(String knummer, String blz)
   {

      this.knummer = knummer;
      this.blz = blz;
   }

   @Override
   public int berechnePruefziffer()
   {
      int summe = 0;

      for (int i = 0; i < 10; i++)
      {
         summe += (int) knummer.charAt(i);
      }

      for (int i = 0; i < 8; i++)
      {
         summe += (int) blz.charAt(i);
      }

      pruefziffer = summe / 18;

      return pruefziffer;
   }

   @Override
   public int getPruefziffer()
   {
      return pruefziffer;
   }
}

