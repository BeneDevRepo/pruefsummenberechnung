package de.fhswf.in.pm.pruefziffern;

/**
 * @author Tobias Holke {@literal holke.tobias@fh-swf.de} on 27.03.19.
 */
public interface Pruefziffer
{
   /**
    * Berechnet die Pr�fziffer.
    *
    * @return Gibt die berechnete Pr�fziffer zur�ck.
    */
   int berechnePruefziffer();

   int getPruefziffer();
}

