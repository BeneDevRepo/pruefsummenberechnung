package de.fhswf.in.pm.pruefziffern;
/**
 * Multiplikationsverfahren zur Pruefziffernberechnung
 *
 * @author Tobias Holke {@literal holke.tobias@fh-swf.de} on 28.03.19.
 */
public class MultiplikationPruefziffer implements Pruefziffer
{
   private String knummer;
   private String blz;
   private int pruefziffer;

   public MultiplikationPruefziffer(String knummer, String blz)
   {

      this.knummer = knummer;
      this.blz = blz;
   }

   @Override
   public int berechnePruefziffer()
   {
      double summe = 1.0;

      for (int i = 0; i < 10; i++)
      {
         summe *= (double) knummer.charAt(i) / 18.0;
      }

      for (int i = 0; i < 8; i++)
      {
         summe *= (double) blz.charAt(i) / 18.0;
      }

      long bits = Double.doubleToLongBits(summe);

      long mantissa = bits & 0x000fffffffffffffL;
      pruefziffer = Integer.valueOf(String.valueOf(mantissa).substring(0, 2));

      return pruefziffer;
   }

   @Override
   public int getPruefziffer()
   {
      return pruefziffer;
   }
}